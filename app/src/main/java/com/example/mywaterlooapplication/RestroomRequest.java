package com.example.mywaterlooapplication;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class RestroomRequest {
    private static RestroomRequest instance;
    private RequestQueue requestQueue;
    //private ImageLoader imageLoader;
    private static Context ctx;

    private RestroomRequest(Context context) {
        ctx = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized RestroomRequest getInstance(Context context) {
        if (instance == null) {
            instance = new RestroomRequest(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

}
