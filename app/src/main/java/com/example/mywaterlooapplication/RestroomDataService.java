package com.example.mywaterlooapplication;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RestroomDataService {

    Context context;
    int latitude = 0;
    int longitude= 0;

    public RestroomDataService(Context context) {
        this.context = context;
    }

    //Callback problem solved:
    public interface VolleyResponseListener {
        void onError(String message);

        void onResponse(ArrayList<LatLng> coordinates);
    }

    public void getRestroomCordinatesByInput(String locationSearchInput, VolleyResponseListener volleyResponseListener) {
        String url = "https://www.refugerestrooms.org/api/v1/restrooms/search.json?query="+ locationSearchInput;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {


                    @Override
                    public void onResponse(JSONArray response) {

                        JSONArray restroomList = response;

                        ArrayList<LatLng> coordinates = new ArrayList<>();

                        for(int i = 0; i<restroomList.length(); i++) {

                            try {
                                JSONObject nearestRestroom = restroomList.getJSONObject(i);
                                double latitude = nearestRestroom.getDouble("latitude");
                                double longitude = nearestRestroom.getDouble("longitude");

                                LatLng latLng = new LatLng(latitude,longitude);
                                coordinates.add(latLng);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                        volleyResponseListener.onResponse(coordinates);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyResponseListener.onError("Error");

            }
        });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        RestroomRequest.getInstance(context).addToRequestQueue(jsonArrayRequest);


    }

    public interface RestroomByInputLocationResponse {
        void onError(String message);

        void onResponse(List<RestroomModel> restroomModels);
    }

    public void getRestroomByInputLocation(String inputLocation,RestroomByInputLocationResponse restroomByInputLocationResponse) {
        List<RestroomModel> restrooms = new ArrayList<>();

        String url = "https://www.refugerestrooms.org/api/v1/restrooms/search.json?query=" +inputLocation;
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {


                try {
                    JSONArray restroomList = response;



                    for(int i = 0; i<restroomList.length(); i++) {

                        RestroomModel restroomInTheArea = new RestroomModel();

                        JSONObject nearestRestroom = restroomList.getJSONObject(i);
                        restroomInTheArea.setId(nearestRestroom.getInt("id"));
                        restroomInTheArea.setName(nearestRestroom.getString("name"));
                        restroomInTheArea.setStreet(nearestRestroom.getString("street"));
                        restroomInTheArea.setCity(nearestRestroom.getString("city"));
                        restroomInTheArea.setState(nearestRestroom.getString("state"));
                        restroomInTheArea.setAccessible(nearestRestroom.getBoolean("accessible"));
                        restroomInTheArea.setUnisex(nearestRestroom.getBoolean("unisex"));
                        restroomInTheArea.setDirections(nearestRestroom.getString("directions"));
                        restroomInTheArea.setComments(nearestRestroom.getString("comment"));
                        restroomInTheArea.setLatitude(nearestRestroom.getDouble("latitude"));
                        restroomInTheArea.setLongitude(nearestRestroom.getDouble("longitude"));
                        restroomInTheArea.setDownvote(nearestRestroom.getInt("downvote"));
                        restroomInTheArea.setUpvote(nearestRestroom.getInt("upvote"));
                        restroomInTheArea.setCountry(nearestRestroom.getString("country"));
                        restroomInTheArea.setChanging_table(nearestRestroom.getBoolean("changing_table"));
                        restroomInTheArea.setEdit_id(nearestRestroom.getInt("edit_id"));
                        restroomInTheArea.setApproved(nearestRestroom.getBoolean("approved"));
                        restrooms.add(restroomInTheArea);
                    }

                    restroomByInputLocationResponse.onResponse(restrooms);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);

            }
        });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        RestroomRequest.getInstance(context).addToRequestQueue(jsonArrayRequest);
    }
}
