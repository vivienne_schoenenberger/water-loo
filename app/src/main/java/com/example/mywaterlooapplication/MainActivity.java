package com.example.mywaterlooapplication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    TextInputEditText inputEditText;
    Button button;
    ListView listView;
    ProgressBar pb;
    SupportMapFragment supportMapFragment;
    FusedLocationProviderClient client;
    private Geocoder geocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        setContentView(R.layout.list_activity);
        geocoder = new Geocoder(this);
        inputEditText = findViewById(R.id.Textinput);
        button = findViewById(R.id.search);
        listView = findViewById(R.id.list);
        pb = findViewById(R.id.progressBar);
        pb.setVisibility(View.INVISIBLE);
        RestroomDataService restroomDataService = new RestroomDataService(MainActivity.this);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pb.setVisibility(View.VISIBLE);
                if (ActivityCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    getLocationOfRestroom();
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
                }
                restroomDataService.getRestroomByInputLocation(inputEditText.getText().toString(), new RestroomDataService.RestroomByInputLocationResponse() {

                    @Override
                    public void onError(String message) {
                        Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onResponse(List<RestroomModel> restroomModels) {
                        ArrayAdapter arrayAdapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, restroomModels);
                        listView.setAdapter(arrayAdapter);
                    }
                });
            }
        });

        supportMapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.google_map);
        client = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            getCurrentLocation();

        } else {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
        }
    }

    private void getLocationOfRestroom() {
        supportMapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                LatLng latLng1 = new LatLng(0.0, 0.0);
                try {
                    List<Address> adresses = geocoder.getFromLocationName(inputEditText.getText().toString(), 1);
                    Address mainAdress = adresses.get(0);
                    latLng1 = new LatLng(mainAdress.getLatitude()
                            , mainAdress.getLongitude());
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng1, 10));
                    googleMap.addMarker(new MarkerOptions().position(latLng1)
                            .icon(BitmapDescriptorFactory
                                    .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                RestroomDataService restroomDataService = new RestroomDataService(MainActivity.this);
                LatLng finalLatLng = latLng1;
                restroomDataService.getRestroomCordinatesByInput(inputEditText.getText().toString(), new RestroomDataService.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                    }
                    @Override
                    public void onResponse(ArrayList<LatLng> coordinates) {

                        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.appicon_round);
                        Bitmap smallMarker = Bitmap.createScaledBitmap(icon, 100, 100, false);

                        ArrayList<String> distances = new ArrayList<>();
                        for (int i = 0; i < coordinates.size(); i++) {
                            String distance = calculateDistance(finalLatLng, coordinates.get(i));
                            distances.add(distance);

                            MarkerOptions markerOptions = new MarkerOptions().position(coordinates.get(i))
                                    .title(distance + " km")
                                    .icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
                            googleMap.addMarker(markerOptions);
                            pb.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });
    }

    private String calculateDistance(LatLng startingPoint, LatLng endingPoint) {
        double lon1 = startingPoint.longitude;
        double lon2 = endingPoint.longitude;
        double lat1 = startingPoint.latitude;
        double lat2 = endingPoint.latitude;
        // The math module contains a function
        // named toRadians which converts from
        // degrees to radians.
        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);
        // Haversine formula
        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2), 2);
        double c = 2 * Math.asin(Math.sqrt(a));
        // Radius of earth in kilometers. Use 3956
        // for miles
        double r = 6371;
        // calculate the result
        double finalDistance = c * r;
        DecimalFormat df = new DecimalFormat("#.##");
        String newFormatDistace = df.format(finalDistance);
        return (newFormatDistace);
    }
    private void getCurrentLocation() {
        @SuppressLint("MissingPermission") Task<Location> task = client.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            pb.setVisibility(View.VISIBLE);
                            String address = "";
                            String city = "";
                            LatLng latLng = new LatLng(location.getLatitude()
                                    , location.getLongitude());
                            MarkerOptions options = new MarkerOptions().position(latLng)
                                    .title("I am Here");
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
                            googleMap.addMarker(options);
                            try {
                                Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                                List<Address> addresses = null;
                                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                                city = addresses.get(0).getLocality();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            RestroomDataService restroomDataService = new RestroomDataService(MainActivity.this);
                            restroomDataService.getRestroomByInputLocation(city, new RestroomDataService.RestroomByInputLocationResponse() {
                                @Override
                                public void onError(String message) {
                                    Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                }
                                @Override
                                public void onResponse(List<RestroomModel> restroomModels) {
                                    ArrayAdapter arrayAdapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_list_item_1, restroomModels);
                                    listView.setAdapter(arrayAdapter);
                                }
                            });
                            restroomDataService.getRestroomCordinatesByInput(city, new RestroomDataService.VolleyResponseListener() {
                                @Override
                                public void onError(String message) {
                                }
                                public void onResponse(ArrayList<LatLng> coordinates) {
                                    Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.appicon_round);
                                    Bitmap smallMarker = Bitmap.createScaledBitmap(icon, 100, 100, false);
                                    for (int i = 0; i < coordinates.size(); i++) {
                                        MarkerOptions markerOptions = new MarkerOptions().position(coordinates.get(i))
                                                .title("Restroom")
                                                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
                                        googleMap.addMarker(markerOptions);
                                    }
                                    pb.setVisibility(View.GONE);
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 44) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCurrentLocation();
            }
        }
    }
}
